# Window Service Delete

Some much useless services ! Auto Update, Licences and everything ...
This script help you delete all the services, easily !
In only few step you'll have those script deleted from your window computer.

### Installation and Use

`run cmd as admin` and execute this command
```sh
$ sc_gestion.bat
```

you might have to select the right directory first.
Our suggestion is for you to have the WSD folder on your desktop and execute these commands just after launching `cmd as admin` 

```sh
$ cd ..
$ cd ..
$ cd Users/[Your Username]/Desktop/WSD
$ sc_gestion.bat
```
You can now browse the WSD folder and read the sc_list.txt, you'll find services Display_Name and Name. Your task is now to write the Name of the services you wish to delete; 
One per Line. 
Then just press the enter key.


### Todos

 - User Interface
 - Sort Services

License
----
CC-BY